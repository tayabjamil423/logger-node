const test = require("node:test");
const assert = require("node:assert");
//
const Logger = require("../lib/Logger");
const lodash = require("lodash");

test("getChild", async (t) => {
  await t.test("getChild should not throw", () => {
    const rootLogger = Logger.getRoot("Root");

    const childLogger = rootLogger.getChild("Child");
  });

  await t.test("getChild should not return nil", () => {
    const rootLogger = Logger.getRoot("Root");

    const childLogger = rootLogger.getChild("Child");

    assert(childLogger !== undefined && childLogger !== null);
  });

  await t.test("childLogger and rootLogger should be linked", (t) => {
    const rootLogger = Logger.getRoot("Root");
    rootLogger.configure({
      isEnabled: true,
      logLevel: 0,
    });

    const childLogger = rootLogger.getChild("Child");

    // rootLogger should have childLogger as a child
    assert.equal(rootLogger.children["Child"], childLogger);

    //  childLogger should have rootLogger as its parent
    assert.equal(childLogger.parent, rootLogger);
  });

  await t.test(
    "childLogger should inherit isEnabled and logLevel properties",
    (t) => {
      const rootLogger = Logger.getRoot("Root");
      rootLogger.configure({
        isEnabled: true,
        logLevel: 0,
      });

      const childLogger = rootLogger.getChild("Child");

      // childLogger should inherit isEnabled and logLevel properties
      assert.equal(childLogger.isEnabled, rootLogger.isEnabled);
      assert.equal(childLogger.logLevel, rootLogger.logLevel);
    }
  );

  await t.test("grandchildLogger and childLogger should be linked", (t) => {
    const rootLogger = Logger.getRoot("Root");

    const childLogger = rootLogger.getChild("Child");
    childLogger.configure({
      isEnabled: true,
      logLevel: 0,
    });

    const grandchildLogger = childLogger.getChild("Grandchild");

    // childLogger should have grandChildLogger as its child
    assert.equal(childLogger.children["Grandchild"], grandchildLogger);
    // grandchildLogger should have childLogger as its parent
    assert.equal(grandchildLogger.parent, childLogger);
  });

  await t.test(
    "grandchildLogger should inherit isEnabled and logLevel properties",
    (t) => {
      const rootLogger = Logger.getRoot("Root");

      const childLogger = rootLogger.getChild("Child");
      childLogger.configure({
        isEnabled: true,
        logLevel: 0,
      });

      const grandchildLogger = childLogger.getChild("Grandchild");

      // Grandchild should inherit isEnabled and logLevel proeprties
      assert.equal(grandchildLogger.isEnabled, grandchildLogger.isEnabled);
      assert.equal(grandchildLogger.logLevel, grandchildLogger.logLevel);
    }
  );

  await t.test(
    "getChild should return current logger if called without namesapce",
    (t) => {
      const rootLogger = Logger.getRoot("Root");

      const childLogger = rootLogger.getChild();

      // childLogger should be the rootLogger
      assert.equal(childLogger, rootLogger);
    }
  );

  await t.test("getChild should throw if invalid namespace", (t) => {
    assert.throws(() => {
      const rootLogger = Logger.getRoot("Root");

      const childLogger = rootLogger.getChild(true);
    });
  });
});
