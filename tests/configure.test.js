const test = require("node:test");
const assert = require("node:assert");
//
const Logger = require("../lib/Logger");
const lodash = require("lodash");

test("configure", async (t) => {
  const getOptions = (logger) => {
    const options = lodash.pick(logger, [
      "isEnabled",
      "logLevel",
      "enabledLogIds",
    ]);
    return options;
  };

  await t.test("configure should not throw", (t) => {
    const rootLogger = Logger.getRoot("App");

    rootLogger.configure();
  });

  await t.test(
    "configure should not configure if no options are passed",
    (t) => {
      const rootLogger = Logger.getRoot("App");

      let initialOptions = getOptions(rootLogger);

      rootLogger.configure();

      let newOptions = getOptions(rootLogger);

      // Initial and new options should be the same
      assert.equal(newOptions.isEnabled, initialOptions.isEnabled);
      assert.equal(newOptions.logLevel, initialOptions.logLevel);
      assert.equal(newOptions.enabledLogIds, initialOptions.enabledLogIds);
    }
  );

  await t.test(
    "configure should not configure if empty options are passed",
    (t) => {
      const rootLogger = Logger.getRoot("App");

      const initialOptions = getOptions(rootLogger);

      rootLogger.configure({});

      const newOptions = getOptions(rootLogger);

      // Initial and new options should be the same
      assert.equal(newOptions.isEnabled, initialOptions.isEnabled);
      assert.equal(newOptions.logLevel, initialOptions.logLevel);
      assert.equal(newOptions.enabledLogIds, initialOptions.enabledLogIds);
    }
  );

  await t.test("configure should configure if options are passed", (t) => {
    const rootLogger = Logger.getRoot("App");

    const initialOptions = getOptions(rootLogger);

    rootLogger.configure({
      isEnabled: false,
      logLevel: 0,
      enabledLogIds: {
        id1: true,
      },
    });

    const newOptions = getOptions(rootLogger);

    // New options' isEnabled property should be different and false
    assert.notEqual(newOptions.isEnabled, initialOptions.isEnabled);
    assert.equal(newOptions.isEnabled, false);

    // New options' logLevel property should be different and 0
    assert.notEqual(newOptions.logLevel, initialOptions.logLevel);
    assert.equal(newOptions.logLevel, 0);

    // New options' enabledLogId should have the "id1" key with the value true
    assert.equal(newOptions.enabledLogIds["id1"], true);
  });

  await t.test("configure should throw if invalid options are passed", (t) => {
    const rootLogger = Logger.getRoot("App");

    // There should be a validation error
    assert.throws(() => {
      rootLogger.configure({
        unknownProperty: true,
      });
    });
  });
});
