# API Reference

## Classes

<dl>
<dt><a href="#Logger">Logger</a></dt>
<dd><p>Logger class.</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#ColorFunc">ColorFunc</a> ⇒ <code>string</code></dt>
<dd></dd>
<dt><a href="#LogDef">LogDef</a></dt>
<dd></dd>
<dt><a href="#LogDefsObj">LogDefsObj</a> : <code>Object.&lt;string, LogDef&gt;</code></dt>
<dd></dd>
<dt><a href="#EnabledLogIds">EnabledLogIds</a> : <code>Object.&lt;string, boolean&gt;</code></dt>
<dd></dd>
<dt><a href="#LoggerOptions">LoggerOptions</a></dt>
<dd></dd>
<dt><a href="#GlobalObj">GlobalObj</a></dt>
<dd></dd>
</dl>

<a name="Logger"></a>

## Logger
Logger class.

**Kind**: global class  

* [Logger](#Logger)
    * _instance_
        * [.defaultLogDefs](#Logger+defaultLogDefs) : [<code>LogDefsObj</code>](#LogDefsObj)
        * [.logDefs](#Logger+logDefs) : [<code>LogDefsObj</code>](#LogDefsObj)
        * [.isRoot](#Logger+isRoot) : <code>boolean</code>
        * [.namespace](#Logger+namespace) : <code>string</code>
        * [.parent](#Logger+parent) : [<code>Logger</code>](#Logger)
        * [.children](#Logger+children) : <code>Object.&lt;String, Logger&gt;</code>
        * [.isEnabled](#Logger+isEnabled) : <code>boolean</code>
        * [.logLevel](#Logger+logLevel) : <code>number</code>
        * [.enabledLogIds](#Logger+enabledLogIds) : [<code>EnabledLogIds</code>](#EnabledLogIds)
        * [.lastLogTime](#Logger+lastLogTime) : <code>number</code>
        * [.root](#Logger+root) ⇒ [<code>Logger</code>](#Logger)
        * [.fullNamespace](#Logger+fullNamespace) ⇒ <code>string</code>
        * [.LogLevels](#Logger+LogLevels) : <code>enum</code>
        * [.configure([options])](#Logger+configure)
        * [.log(type, text, ...args)](#Logger+log) ⇒ <code>boolean</code>
        * [.logId(id, type, text, ...args)](#Logger+logId) ⇒ <code>boolean</code>
        * [.setLogIdEnabled(logId, [isEnabled])](#Logger+setLogIdEnabled)
        * [.getChild(namespace)](#Logger+getChild) ⇒ [<code>Logger</code>](#Logger)
        * [.getDescendant(...namespaces)](#Logger+getDescendant) ⇒ [<code>Logger</code>](#Logger)
    * _static_
        * [.getRootGlobal(namespace)](#Logger.getRootGlobal) ⇒ [<code>Logger</code>](#Logger)
        * [.getRoot(namespace)](#Logger.getRoot) ⇒ [<code>Logger</code>](#Logger)

<a name="Logger+defaultLogDefs"></a>

### logger.defaultLogDefs : [<code>LogDefsObj</code>](#LogDefsObj)
Default log definitions.

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+logDefs"></a>

### logger.logDefs : [<code>LogDefsObj</code>](#LogDefsObj)
Log definitions.

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+isRoot"></a>

### logger.isRoot : <code>boolean</code>
Indicates whether this is the root Logger.

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+namespace"></a>

### logger.namespace : <code>string</code>
The log namespace.

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+parent"></a>

### logger.parent : [<code>Logger</code>](#Logger)
A reference to the parent Logger.

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+children"></a>

### logger.children : <code>Object.&lt;String, Logger&gt;</code>
A list of references to child Loggers.

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+isEnabled"></a>

### logger.isEnabled : <code>boolean</code>
Determines if logging is enabled.

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+logLevel"></a>

### logger.logLevel : <code>number</code>
Log level.

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+enabledLogIds"></a>

### logger.enabledLogIds : [<code>EnabledLogIds</code>](#EnabledLogIds)
Enabled log IDs.

If 'null' then all log IDs will be enabled.

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+lastLogTime"></a>

### logger.lastLogTime : <code>number</code>
The last log timestamp.

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+root"></a>

### logger.root ⇒ [<code>Logger</code>](#Logger)
Gets the root Logger instance.

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+fullNamespace"></a>

### logger.fullNamespace ⇒ <code>string</code>
Gets the full namespace of this Logger.

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+LogLevels"></a>

### logger.LogLevels : <code>enum</code>
**Kind**: instance enum of [<code>Logger</code>](#Logger)  
**Access**: public  
<a name="Logger+configure"></a>

### logger.configure([options])
Configures this Logger.

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Access**: public  

| Param | Type | Description |
| --- | --- | --- |
| [options] | [<code>LoggerOptions</code>](#LoggerOptions) | Options to set. |

<a name="Logger+log"></a>

### logger.log(type, text, ...args) ⇒ <code>boolean</code>
Logs text to the output.

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Returns**: <code>boolean</code> - - If the log was permitted.  
**Access**: public  

| Param | Type | Description |
| --- | --- | --- |
| type | <code>string</code> | Log type defined in logDefs. |
| text | <code>string</code> | Format text. |
| ...args | <code>any</code> | Format text args. |

<a name="Logger+logId"></a>

### logger.logId(id, type, text, ...args) ⇒ <code>boolean</code>
Logs text to the output with an ID.

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Returns**: <code>boolean</code> - If the log was permitted.  
**Access**: public  

| Param | Type | Description |
| --- | --- | --- |
| id | <code>string</code> | ID of this log. |
| type | <code>string</code> | Log type defined in logDefs. |
| text | <code>string</code> | Format text. |
| ...args | <code>any</code> | Format text args. |

<a name="Logger+setLogIdEnabled"></a>

### logger.setLogIdEnabled(logId, [isEnabled])
Sets a log ID enabled.

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Access**: public  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| logId | <code>string</code> |  | The log ID. |
| [isEnabled] | <code>boolean</code> | <code>true</code> | Determines whether to enable to disable the log ID. |

<a name="Logger+getChild"></a>

### logger.getChild(namespace) ⇒ [<code>Logger</code>](#Logger)
Gets or creates a child Logger.

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Access**: public  

| Param | Type | Description |
| --- | --- | --- |
| namespace | <code>string</code> | Child Logger namespace. |

<a name="Logger+getDescendant"></a>

### logger.getDescendant(...namespaces) ⇒ [<code>Logger</code>](#Logger)
Gets or creates a descendant Logger.

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Access**: public  

| Param | Type | Description |
| --- | --- | --- |
| ...namespaces | <code>string</code> | A list of descendant Logger namespacess. |

<a name="Logger.getRootGlobal"></a>

### Logger.getRootGlobal(namespace) ⇒ [<code>Logger</code>](#Logger)
Gets or creates a Global root Logger instance.

**Kind**: static method of [<code>Logger</code>](#Logger)  
**Access**: public  

| Param | Type | Description |
| --- | --- | --- |
| namespace | <code>string</code> | The log namespace. |

<a name="Logger.getRoot"></a>

### Logger.getRoot(namespace) ⇒ [<code>Logger</code>](#Logger)
Gets a root Logger instance.

**Kind**: static method of [<code>Logger</code>](#Logger)  
**Access**: public  

| Param | Type |
| --- | --- |
| namespace | <code>string</code> | 

<a name="ColorFunc"></a>

## ColorFunc ⇒ <code>string</code>
**Kind**: global typedef  
**Returns**: <code>string</code> - - The coloured text.  

| Param | Description |
| --- | --- |
| text | The text to color. |

<a name="LogDef"></a>

## LogDef
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| tag | <code>string</code> | Tag used in the log namespace. |
| headerColorFunc | [<code>ColorFunc</code>](#ColorFunc) | Function to color the header text. |
| textColorFunc | [<code>ColorFunc</code>](#ColorFunc) | Function to color the format text. |
| outputFunc | <code>function</code> | Function to output the logged text. |
| level | <code>LogLevels</code> | Log level. |

<a name="LogDefsObj"></a>

## LogDefsObj : <code>Object.&lt;string, LogDef&gt;</code>
**Kind**: global typedef  
<a name="EnabledLogIds"></a>

## EnabledLogIds : <code>Object.&lt;string, boolean&gt;</code>
**Kind**: global typedef  
<a name="LoggerOptions"></a>

## LoggerOptions
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| [isEnabled] | <code>boolean</code> | Determines whether logging is enabled. |
| [logLevel] | <code>LogLevels</code> | The minimum log level. |
| [enabledLogIds] | [<code>EnabledLogIds</code>](#EnabledLogIds) | Enabled log IDs. |

<a name="GlobalObj"></a>

## GlobalObj
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| rootLoggerCache | <code>Object.&lt;string, Logger&gt;</code> | 