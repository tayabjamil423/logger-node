const util = require("util");

function inspectSingleLine(value) {
  const inspected = util
    .inspect(value, {
      showHidden: false,
      depth: null,
      colors: true,
    })
    .split("\n")
    .map((str) => str.trim())
    .join(" ");
  return inspected;
}

function inspectMultiLine(value) {
  const inspected = util.inspect(value, {
    showHidden: false,
    depth: null,
    colors: true,
  });
  return inspected;
}

const formatters = {};
formatters["%o"] = inspectSingleLine;
formatters["%O"] = inspectMultiLine;

const defautFormatter = inspectSingleLine;

function formatStr(str, args, processText) {
  let formatted = null;

  // Split str by placeholders
  const regex = /(%[a-zA-Z]+)/g;
  let tokens = str.split(regex);

  // Remove empty values
  tokens = tokens.filter((token) => {
    token = token.trim();
    return token && token.length > 0;
  });

  let placeholderIndex = 0;
  tokens = tokens.map((token) => {
    // If token is text
    if (!token.startsWith("%")) {
      // If a process function is supplied, apply it on the text
      if (processText && typeof processText === "function") {
        token = processText(token);
      }

      // Return the text
      return token;
    }

    // Get current arg
    const arg = args[placeholderIndex];
    // If there aren't enough args to substitute the placeholders
    if (arg === undefined) {
      throw new Error("Not enough args");
    }

    placeholderIndex++;

    // Get formatter for the placeholder or default
    const formatter = formatters[token] || defautFormatter;
    // Format arg and return
    const formatted = formatter(arg);
    return formatted;
  });

  formatted = tokens.join("");
  return formatted;
}

module.exports = formatStr;
