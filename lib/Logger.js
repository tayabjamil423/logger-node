const util = require("util");
//
const Joi = require("joi");
const c = require("ansi-colors");
const now = require("performance-now");
const lodash = require("lodash");
//
const formatStr = require("./formatStr");

/**
 * @callback ColorFunc
 * @param text - The text to color.
 * @returns {string} - The coloured text.
 */

/**
 * @typedef LogDef
 * @property {string} tag - Tag used in the log namespace.
 * @property {ColorFunc} headerColorFunc - Function to color the header text.
 * @property {ColorFunc} textColorFunc - Function to color the format text.
 * @property {Function} outputFunc - Function to output the logged text.
 * @property {LogLevels} level - Log level.
 */

/**
 * @typedef {Object.<string, LogDef>} LogDefsObj
 */

/**
 * @typedef {Object.<string, boolean>} EnabledLogIds
 */

/**
 * @typedef LoggerOptions
 * @property {boolean} [isEnabled] - Determines whether logging is enabled.
 * @property {LogLevels} [logLevel] - The minimum log level.
 * @property {EnabledLogIds} [enabledLogIds] - Enabled log IDs.
 */

/**
 * @typedef GlobalObj
 * @property {Object.<string, Logger>} rootLoggerCache
 */

/**
 * @class
 * @classdesc Logger class.
 */
class Logger {
  /**
   * @enum {number}
   *
   * @public
   */
  static LogLevels = {
    INFO: 0,
    WARN: 1,
    ERR: 2,
  };

  /**
   * Default log definitions.
   *
   * @type {LogDefsObj}
   *
   * @public
   */
  static defaultLogDefs = {
    info: {
      tag: "Info",
      headerColorFunc: c.blue.bold,
      textColorFunc: c.white,
      outputFunc: console.log,
      level: Logger.LogLevels.INFO,
    },
    success: {
      tag: "Success",
      headerColorFunc: c.greenBright.bold,
      textColorFunc: c.green,
      outputFunc: console.log,
      level: Logger.LogLevels.INFO,
    },
    warn: {
      tag: "Warning",
      headerColorFunc: c.yellowBright.bold,
      textColorFunc: c.yellow,
      outputFunc: console.warn,
      level: Logger.LogLevels.WARN,
    },
    err: {
      tag: "Error",
      headerColorFunc: c.redBright.bold,
      textColorFunc: c.red,
      outputFunc: console.log,
      level: Logger.LogLevels.ERR,
    },
  };

  /**
   * Log definitions.
   *
   * @type {LogDefsObj}
   *
   * @public
   */
  logDefs = undefined;

  /**
   * Indicates whether this is the root Logger.
   *
   * @type {boolean}
   *
   * @public
   */
  isRoot = null;

  /**
   * The log namespace.
   *
   * @type {string}
   *
   * @public
   */
  namespace = null;

  /**
   * A reference to the root Logger.
   *
   * @type {Logger}
   *
   * @private
   */
  _root = undefined;

  /**
   * A reference to the parent Logger.
   *
   * @type {Logger}
   *
   * @public
   */
  parent = undefined;

  /**
   * A list of references to child Loggers.
   *
   * @type {Object.<String, Logger>}
   *
   * @public
   */
  children = {};

  /**
   * Determines if logging is enabled.
   *
   * @type {boolean}
   *
   * @public
   */
  isEnabled = true;

  /**
   * Log level.
   *
   * @type {number}
   *
   * @public
   */
  logLevel = Logger.LogLevels.WARN;

  /**
   * Enabled log IDs.
   *
   * If 'null' then all log IDs will be enabled.
   *
   * @type {EnabledLogIds}
   *
   * @public
   */
  enabledLogIds = {};

  /**
   * The last log timestamp.
   *
   * @type {number}
   *
   * @public
   */
  lastLogTime = null;

  constructor() {}
  
  /**
   * Gets or creates a Global root Logger instance.
   *
   * @param {string} namespace - The log namespace.
   *
   * @returns {Logger}
   *
   * @public
   */
  static getRootGlobal(namespace) {
    Joi.assert(namespace, Joi.string().required());

    const globalObj = Logger._getGlobalObj();
    const rootCache = globalObj.rootLoggerCache;

    // If there's an existing root Logger in the global cache then return it.
    const existingRoot = rootCache[namespace];
    if (existingRoot) {
      return existingRoot;
    }

    // Otherise create a new root Logger instance.
    const newRoot = this._getRoot(namespace);
    rootCache[namespace] = newRoot;
    return newRoot;
  }

  /**
   * Gets a root Logger instance.
   *
   * @param {string} namespace
   *
   * @returns {Logger}
   *
   * @public
   */
  static getRoot(namespace) {
    const root = this._getRoot(namespace);
    return root;
  }

  /**
   *
   * @param {string} namespace
   *
   * @returns {Logger}
   *
   * @private
   */
  static _getRoot(namespace) {
    Joi.assert(namespace, Joi.string().required());

    const logger = new Logger();
    logger.namespace = namespace;
    logger.isRoot = true;
    logger.logDefs = Logger.defaultLogDefs;

    return logger;
  }

  /**
   * @returns {GlobalObj}
   *
   * @private
   */
  static _getGlobalObj() {
    let globalObj = lodash.get(global, "_ph.Logger", null);
    const isGlobalInit = !lodash.isNil(globalObj);

    if (!isGlobalInit) {
      globalObj = {
        rootLoggerCache: {
          // empty
        },
      };

      lodash.set(global, "_ph.Logger", globalObj);
    }

    return globalObj;
  }

  /**
   * Gets the root Logger instance.
   *
   * @returns {Logger}
   *
   * @public
   */
  get root() {
    const root = this.isRoot ? this : this._root;
    return root;
  }

  /**
   * Gets the full namespace of this Logger.
   *
   * @returns {string}
   *
   * @public
   */
  get fullNamespace() {
    let fullNamespace = `${this.namespace}`;
    let parentLogger = this.parent;
    while (parentLogger) {
      fullNamespace = `${parentLogger.namespace}` + ":" + `${fullNamespace}`;
      parentLogger = parentLogger.parent;
    }
    return fullNamespace;
  }

  //

  static loggerOptionsSchema = Joi.object({
    isEnabled: Joi.bool().optional(),
    logLevel: Joi.number().optional(),
    enabledLogIds: Joi.object().unknown().allow(null).optional(),
  });
  /**
   * Configures this Logger.
   *
   * @param {LoggerOptions} [options] - Options to set.
   *
   * @public
   */
  configure(options) {
    options = Joi.attempt(options, Logger.loggerOptionsSchema);

    lodash.assign(this, options);
  }

  /**
   *
   * Logs text to the output.
   *
   * @param {string} type - Log type defined in logDefs.
   * @param {string} text - Format text.
   * @param  {...any} args - Format text args.
   *
   * @returns {boolean} - If the log was permitted.
   *
   * @public
   */
  log(type, text, ...args) {
    const canLog = this._canLog(type);
    if (!canLog) {
      return false;
    }

    this._log(type, text, ...args);
    return true;
  }

  /**
   * Logs text to the output with an ID.
   *
   * @param {string} id - ID of this log.
   * @param {string} type - Log type defined in logDefs.
   * @param {string} text - Format text.
   * @param  {...any} args - Format text args.
   *
   * @returns {boolean} If the log was permitted.
   *
   * @public
   */
  logId(id, type, text, ...args) {
    Joi.assert(id, Joi.string().required());

    const canLog = this._canLog(type, id);
    if (!canLog) {
      return false;
    }

    this._log(type, text, ...args);
    return true;
  }

  /**
   * Sets a log ID enabled.
   *
   * @param {string} logId  - The log ID.
   * @param {boolean} [isEnabled=true] - Determines whether to enable to disable the log ID.
   *
   * @public
   */
  setLogIdEnabled(logId, isEnabled = true) {
    const enabledLogIds = this.enabledLogIds;
    enabledLogIds[logId] = isEnabled;
  }

  /**
   * Gets or creates a child Logger.
   *
   * @param {string} namespace - Child Logger namespace.
   *
   * @returns {Logger}
   *
   * @public
   */
  getChild(namespace) {
    if (!namespace || namespace.trim() == "") {
      return this;
    }

    const existingChild = this.children[namespace];
    if (existingChild) {
      return existingChild;
    }

    const newLogger = new Logger(namespace);
    newLogger.isRoot = false;
    newLogger._root = this.root;
    newLogger.parent = this;
    newLogger.namespace = namespace;
    newLogger.isEnabled = this.isEnabled;
    newLogger.logLevel = this.logLevel;

    // console.log("Created child:", newLogger);

    this.children[namespace] = newLogger;

    return newLogger;
  }

  /**
   * Gets or creates a descendant Logger.
   *
   * @param  {...string} namespaces - A list of descendant Logger namespacess.
   *
   * @returns {Logger}
   *
   * @public
   */
  getDescendant(...namespaces) {
    namespaces = Joi.attempt(
      namespaces,
      Joi.array().items(Joi.string().allow(null).optional()).sparse()
    );

    let currLogger = this;
    for (let namespace of namespaces) {
      if (!namespace) {
        return currLogger;
      }
      currLogger = currLogger.getChild(namespace);
    }
    return currLogger;
  }

  /**
   * @returns {boolean}
   *
   * @private
   */
  _isEnabled() {
    const isSelfEnabled = this.isEnabled;
    if (!isSelfEnabled) {
      return false;
    }

    // Recurse upward into ancestors
    const parent = this.parent;
    if (parent) {
      const isParentEnabled = parent._isEnabled();
      return isParentEnabled;
    }

    return true;
  }

  /**
   * @param {string} type
   *
   * @returns {boolean}
   *
   * @private
   */
  _isLogLevelEnabled(type) {
    const root = this.root;
    const logDef = root.logDefs[type];
    const requestedLogLevel = logDef.level;

    // console.log("this.logLevel:", this.logLevel, "requestedLogLevel:", requestedLogLevel);

    const isLogLevelEnabled = requestedLogLevel >= this.logLevel;
    if (!isLogLevelEnabled) {
      return false;
    }

    // Recurse upward into ancestors
    const parent = this.parent;
    if (parent) {
      const isParentLogLevelEnabled = parent._isLogLevelEnabled(type);
      return isParentLogLevelEnabled;
    }

    return true;
  }

  /**
   *
   * @param {string} logId
   *
   * @returns {boolean}
   *
   * @private
   */
  _isLogIdEnabled(logId) {
    const enabledLogIds = this.enabledLogIds;
    const isSelfLogIdEnabled =
      lodash.isNil(enabledLogIds) || enabledLogIds[logId] !== false;
    if (!isSelfLogIdEnabled) {
      return false;
    }

    // Recurse upward into ancestors
    const parent = this.parent;
    if (parent) {
      const isParentLogIdEnabled = parent._isLogIdEnabled(logId);
      return isParentLogIdEnabled;
    }

    return true;
  }

  /**
   *
   * @param {string} type
   * @param {string} [logId=null]
   *
   * @returns {boolean}
   * 
   * @private
   */
  _canLog(type, logId = null) {
    const isEnabled = this._isEnabled();
    const isLogLevelEnabled = this._isLogLevelEnabled(type);
    const isLogIdEnabled = logId ? this._isLogIdEnabled(logId) : true;

    // console.log(
    //   "isEnabled:",
    //   isEnabled,
    //   "isLogLevelEnabled:",
    //   isLogLevelEnabled,
    //   "isLogIdEnabled",
    //   isLogIdEnabled
    // );

    const canLog = isEnabled && isLogLevelEnabled && isLogIdEnabled;

    return canLog;
  }

  /**
   * 
   * @param {string} type 
   * @param {string} text 
   * @param  {...any} args 
   * 
   * @private
   */
  _log(type, text, ...args) {
    // Validate params
    const rootLogger = this.root;
    // console.log("rootLogger:", rootLogger);
    const logDefs = rootLogger.logDefs;

    // Validate type
    const logDefKeys = Object.keys(logDefs);
    Joi.assert(
      type,
      Joi.string()
        .valid(...logDefKeys)
        .required()
    );

    // Validate text
    Joi.assert(text, Joi.string().required());

    // Calculate elapsed time since last log
    const lastLogTime = rootLogger.lastLogTime || now();
    const curLogTime = now();
    const elaspedTime = curLogTime - lastLogTime;
    rootLogger.lastLogTime = curLogTime;

    const logDef = logDefs[type];
    const { tag, headerColorFunc, textColorFunc, outputFunc } = logDef;

    // Form header
    let fullNamespace = this.fullNamespace + ":" + `${tag}`;
    fullNamespace = headerColorFunc(fullNamespace);

    // Format text
    let textFormatted = formatStr(text, args, textColorFunc);

    // Form log text
    let logText = textFormatted
      .split("\n")
      .map((line) => {
        const mapped = "  " + fullNamespace + " - " + line;
        return mapped;
      })
      .join("\n");

    logText += c.white.italic("  +" + elaspedTime.toFixed(3) + "ms");

    // Call the output function
    outputFunc(logText);
  }
}

module.exports = Logger;
