# Logger

## Installation
**Run command:**
```
npm install https://gitlab.com/tayabjamil423/logger-node.git
```

**Import:**
```js
const Logger = require("@ph/Logger");
```

## API Reference

[View here](./docs/Logger.md)

## TODO:
- Finish writing tests

